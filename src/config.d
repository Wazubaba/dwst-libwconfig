module dwst.config;

import std.stdio: File;
import std.string: indexOf, split, strip, splitLines;
import std.conv: to;

// Publicly imported to simplify responding to throws from LoadConfig()
public import std.exception: ErrnoException;

/++
	Library that implements a basic configuration file format.
	Format is a basic `key = val` affair, which gets stored in an
	associative array for easy searching via the `in` keyword.
++/

/// Error thrown if parsing fails.
class ConfigParserError: Exception
{
	@safe nothrow this(string msg)
	{
		super(msg);
	}
}

/// Config data structure using an associative array of string's
alias Config = string[string];

/++
	Load a config file from path.
	Takes:
		string path - path to the config file to read
	Returns:
		Config rhs - Config containing an associative array of string key and value pairs
	Throws:
		ErrnoException - if target file does not exist
		ConfigParserError - if target file cannot be properly parsed
++/
Config LoadConfig(string path)
{
	Config rhs;
	File fp = File(path);

	size_t lineNo = 0;

	foreach (line; fp.byLine())
	{
		lineNo++;
		if (line.indexOf('=') != -1)
		{
			line = line.split("#")[0]; // Strip comments off

			// In case it is a commented-out line, test to make sure we still have a valid data bit or not
			if (line.indexOf('=') == -1) continue;

			string key, val;
			if (line.split("=").length < 2) throw new ConfigParserError("Malformed line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line));
			key = to!string(line.split("=")[0]).strip();
			val = to!string(line.split("=")[1]).strip();

			rhs[key] = val;
		} else
		if (line.strip.length > 0 || line.strip.indexOf('#') == 0) // Is not a random blank line nor a line comment
			throw new ConfigParserError("Malformed line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line));
	}

	return rhs;
}

/++
	Write configuration data to a target file.
	Takes:
		Config cfg - configuration data to write
		string path - path to the config file to write to
	Throws:
		ErrnoException - if target file cannot be written to
++/
void WriteConfig(Config cfg, string path)
{
	File fp = File(path, "w");
	foreach (key, val; cfg)
		fp.writeln(key ~ " = " ~ val);
}

unittest
{
	import std.stdio: writeln, write;
	import std.exception: enforce;

	writeln("===dwst.config unittest begins===");

	write("\tGenerating test data");
	Config data;
	data["testkey"] = "testval";
	data["moardata"] = "ohaideru";

	writeln("    ok");

	write("\tWriting test data: __unittest__.cfg");
	WriteConfig(data, "__unittest__.cfg");

	writeln("    ok");

	write("\tLoading test data: __unittest__.cfg");
	auto cfg = LoadConfig("__unittest__.cfg");
	enforce(cfg.length == 2, "    ERR: incorrect length for saved data found");
	enforce("testkey" in cfg, "    ERR: could not find testkey in test data");
	enforce("moardata" in cfg, "    ERR: could not find moardata in test data");
	enforce(cfg["testkey"] == "testval", "    ERR: incorrect data saved for key 0");
	enforce(cfg["moardata"] == "ohaideru", "    ERR: incorrect data saved for key 1");

	writeln("    ok");
	writeln("===Test successful===");
}

