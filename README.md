# l i b c o n f i g
A stupidly simple config file format for rapid development usage.

# C O M P I L I N G
Just run `make` for the default build.
The makefile supports the following primary rules:
* all - build every rule
* release - (default rule) builds the library in release mode with optimizations
* debug - builds a debugging build of the library, containing debugging
	symbols and with debug code enabled
* docs - build inline documentation
* test - builds unittests with debugging symbols
* clean - deletes generated build files
* package - constructs bundles for use in providing pre-built packages
* install - installs the library for system-wide use, supports DESTDIR setting
* uninstall - uninstalls the library, supports DESTDIR setting because why not

You can alternatively suffix `release`, `debug`, and `test` with `32` or `64` to
restrict builds to those architectures.

